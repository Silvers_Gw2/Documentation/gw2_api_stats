# Forging
This is data obtained by myself from forging, my own private stats made public.

## Precurser

```json
[
  {
    "ISO": "2018-04-02T12:15:43.097Z",
    "sell": 9977,
    "name": "Carrion Pearl Crusher",
    "ID": 15514
  }
]
```

This returns a list of my pre forgings from level 76+ exotics.  
Contains time of forging, id and price of output.  
Price is the sell price at the time of forging.  
Some older entries contain a name element as that's how I differentiated them for a while.  
And yes there is a typo.  

Example  
`https://api.silveress.ie/gw2_stats/v1/forging/precurser`

Query:

* Beautify

## Minis

````json
[
  {
    "ISO": "2016-10-10T00:00:00Z",
    "name": "Mini High Inquisitor Maut",
    "sell": 100000.1765
  }
]
````

This returns a list of my mini forgings from level rare minis.  
Like with the pre list it has date and price, however due to each mini being unique name wise I didn't switch to using ID.


Example  
`https://api.silveress.ie/gw2_stats/v1/forging/minis`


Query:

* Beautify
