# Queries

Unlike my main Gw2 API this one just has one parameter right now - beautify.  
One possible future query could be a start/end but for the moment that can be put into place client side.

### Queries
Queries are done in the format of:  
``?query1=query1Parameter&query2=query2Parameter&..&queryN=queryNParameter``

### Parameters
Parameters are comma separated strings:  
``?query1=parm1,parm2,parm3,...,parmN``


## Beautify

This controls the format of outputted JSON.  
Default is ``human``  
Other option is ``min``

Example  
`https://api.silveress.ie/gw2_stats/v1/forging/precurser?beautify=human`  
`https://api.silveress.ie/gw2_stats/v1/forging/precurser?beautify=min`
